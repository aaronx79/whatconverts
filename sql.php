<?php
namespace sql;
class sql{
	const USER = "**********";
	const PASS = "**********";
	const DATABASE = "*****";
	const SERVER = "******";
	private $conn = "";
	private $values = array();
	private $query = "";
	private $result = "";

	private function connect()
	{
		$this->conn = new \mysqli(self::SERVER, self::USER, self::PASS, self::DATABASE);
		$this->testConnection();
	}

	private function testConnection()
	{
		if($this->conn->connect_error) {
  			die("Connection failed: " . $this->conn->connect_error);
		}
	}


	function __construct($query = "", $values = array())
	{
		//values MUST be passed as associative array
		self::preCheck($query,$values);
		$this->connect();
		$this->query = $query;
		if(count($values)>0){
			$this->values = $values;
		}
		$this->prepareFunc();
	}


	private static function preCheck($query,$values)
	{
		if($query == "test"){
			echo "<b>Test scenario entered.</b>";
		}
		if($query == ""){
			self::error(0); //check obvious errors before attempting connection
		}
	}

	private static function error($error = 0)
	{
		$alert = self::errorStrings();
		die("ERROR: ".$alert[$error]);
	}

	private static function errorStrings()
	{
		return array(
			0 => "no query string entered",
			1 => "no values passed",
			2 => "no arrays allowed to be passed",
			3 => "null type value passed",
			4 => "resource variable found, we don't allow that."
		);
	}

	private function prepareFunc()
	{
		if(count($this->values)>0){
			$stmt = $this->conn->prepare($this->query);
			$types = $this->detectTypes();
			//time for "unpacking" operator which is "..."
			$stmt->bind_param($types, ...array_values($this->values));
			$stmt->execute();
			$stmt->bind_result(...array_keys($this->values));
			return $stmt->insert_id;
		}else{
			//normal query execute.
			$this->result = $this->conn->query($this->query);
			return $this->conn->insert_id;
		}
	}

	private function detectTypes()
	{
		$types = "";
		foreach($this->values as $data){
		  if (is_array($data)){
		    // array
		    self::error(2);
		  } else if (is_string($data)) {
		    // string
		  	$types .= "s";
		  } else if (is_int($data)) {
		    // int
		    $types .= "i";
		  } else if (is_float($data)) {
		    // float
		    $types .= "d";
		  } else if (is_bool($data)) {
		    // bool
		    $types .= "s";
		  } else if (is_object($data)) {
		  	$types .= "b";
		    // object/blob?
		  } else if (is_resource($data)) {
		    // resource
		    self::error(4);
		  } else {
		    // null/invalid type
		    self::error(3);
		  }
		}
		return $types;
	}

	public function getObj(){
		return $this->result;
	}

	public function rows(){
		return $this->result->num_rows;
	}

	public function data(){
		return $this->result->fetch_assoc();
	}


}



?>